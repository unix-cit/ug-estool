#!/usr/bin/python3

import os
import sys
import requests
import argparse
import jmespath
import json
import re
import random
import operator
from itertools import groupby
from datetime import datetime, timedelta, date
import libcbr

URLBASE = "http://127.0.0.1:9200"
DEFAULT_REPOSITORY_BASE_PATH = "/elasticsearch_saves/base"
DEFAULT_REPOSITORY_NAME = "dump"
# delete-old-index
MAX_AGE_DEFAULT=370
MAX_AGE_BY_PREFIX={"vmware":15,"it-firewall":183, "it-ddi":183, "it-csp-report-only":7, "it-csp-violation":7 }
PREFIX_DELETE_OLD_INDEX = ["f5-lab", "f5-prod", "f5-test", "it-aladin-1", "it-bmc-cm", "it-csp", "it-ddi", 
                           "it-firewall", "it-network", "it-prod-matlab", "it-prod-mathematica", "it-voip", 
                           "logstash", "vmware" ]
# RE_DELETE_OLD_INDEX=re.compile("^f5-lab-.*|^f5-prod-.*|f5-test-.*|it-netwit-voip-.*|logstash-.*|it-prod-matlab-.*|")
RE_STR_DELETE_OLD_INDEX = "^" + "-.*|^".join(PREFIX_DELETE_OLD_INDEX) + "-.*"
RE_DELETE_OLD_INDEX = re.compile(RE_STR_DELETE_OLD_INDEX)
DELETE_OLD_INDEX_MAX_ELEMENTS = len(PREFIX_DELETE_OLD_INDEX)*2+10

LNODE = ["lunielkes01", "lunielkes02", "lunielkes03"]


# ENABLE_COLOR=True

def print_color(*a, **kw):
    colorize_text(*a, **kw)


def colorize_text(*a, **kw):
    bcolors = {"HEADER": '\033[95m',
               "OKBLUE": '\033[94m',
               "OKGREEN": '\033[92m',
               "WARNING": '\033[93m',
               "FAIL": '\033[91m',
               "ENDC": '\033[0m'}
    colorname = kw.get("color", "ENDC")
    color = bcolors[colorname.upper()]
    if "color" in kw:
        kw.pop("color")
    # prefix & main
    tmpkw = kw.copy()
    tmpkw["end"] = ""
    print(color, **tmpkw)
    print(*a, **tmpkw)
    # suffix
    print(bcolors["ENDC"], **kw)


def create_default_snapshot_name():
    return datetime.now().strftime("%Y.%m.%d_%Hh%Mm%Ss")


class Api:
    def get(path):
        url = "{}/{}".format(URLBASE, path)
        r = requests.get(url)
        return r.json()

    def put(path, data):
        url = "{}/{}".format(URLBASE, path)
        r = requests.put(url, data)
        return r.json()

    def delete(path):
        url = "{}/{}".format(URLBASE, path)
        r = requests.delete(url)
        return r.json()


class Shard:
    def __init__(self, index=None, shard=None, prirep=None, state=None, docs=None, store=None, ip=None, node=None):
        self.index = index
        self.shard = shard
        self.prirep = prirep
        self.state = state
        self.docs = docs
        self.store = store
        self.ip = ip
        self.node = node

    def reallocate(self, node):
        url = "{}/{}".format(URLBASE, "_cluster/reroute")
        data = {"index": self.index,
                "shard": self.shard,
                "allow_primary": True,
                "node": node}
        payload = {"commands": [{"allocate": data}]}
        print_color("Reallocate to node the {}.".format(node, self), color="okblue")
        print("reroute to node({}) the {}".format(node, self))
        r = requests.post(url, data=json.dumps(payload))
        if r.status_code != 200:
            print_color("Unable to reallocate", color="FAIL")
            print_color("The output is:", color="okblue")
            print_color(r.content)
            raise Exception("Reallocate method was never fully tested. Exit !")

    def __repr__(self):
        return "Shard(index='{s.index}', shard='{s.shard}', prirep='{s.prirep}',…)".format(s=self)


class ShardFactory:
    path = "_cat/shards"

    @classmethod
    def get(cls):
        r = requests.get(URLBASE + '/' + cls.path + "?v")
        # index                     shard prirep state         docs    store ip           node        $
        #  f5-prod-2017.07.31        1     r      STARTED       1202  468.2kb 10.194.11.74 lunielkes03 $
        # f5-prod-2017.07.31        1     p      STARTED       1202    497kb 10.194.11.85 lunielkes02 $
        # ...
        lline = r.content.decode("utf-8").splitlines()
        #
        lkey = (lline[0].split())
        lshard = []
        for line in lline[1:]:
            lvalue = line.split()
            dshard = dict(zip(lkey, lvalue))
            shard = Shard(**dshard)
            lshard.append(shard)
        return lshard


class RoutingAllocation:
    path = "_cluster/settings"

    @classmethod
    def get(cls):
        r = Api.get(cls.path)
        return jmespath.search("transient.cluster.routing.allocation.enable", r)

    @classmethod
    def set(cls, value):
        if not value in ["all", "none"]:
            raise ValueError("argument(enable_or_disable) must be in ['all','none']")
        data = """\
{{
    "transient": {{
        "cluster.routing.allocation.enable" : "{}"
    }}
}}""".format(value)
        return Api.put(cls.path, data)


def get_cluster_health(**kv):
    laccepted_key = ["level", "wait_for_status"]
    lquestion_mark = []
    for key in kv.keys():
        if key not in laccepted_key:
            raise KeyError("key({}) should be in {}".format(key, str(laccepted_key)))
        lquestion_mark.append("{}={}".format(key, kv[key]))
    path = "_cluster/health" + "?" + ",".join(lquestion_mark)
    return Api.get(path)


class RepositoryFactory:
    path = "_snapshot"

    @classmethod
    def list(cls):
        ret = Api.get(cls.path)
        lret = []
        for repo_json in ret.items():
            lret.append(Repository(repo_json))
        return lret

    @classmethod
    def get(cls, repo_name):
        for repo in cls.list():
            if repo_name == repo.name:
                return repo
        return None

    @classmethod
    def create(cls, repo_name=DEFAULT_REPOSITORY_NAME):
        location = os.path.join(DEFAULT_REPOSITORY_BASE_PATH, repo_name)
        lrepo = cls.list()
        if lrepo:
            if repo_name in lrepo:
                raise Exception("Repository name ({}) already exists. Unable to create it!".format(repo_name))
        data = '''\
{{
    "type": "fs",
    "settings": {{
        "compress": "true",
        "location": "{}"
    }}
}}'''.format(location)
        return Api.put(cls.path + "/" + repo_name, data)

    @classmethod
    def delete(cls, repo_name):
        return Api.delete(cls.path + "/" + repo_name)


class Repository:
    def __init__(self, json):
        self.json = json
        self.name = json[0]

    def __repr__(self):
        return "Repository({})".format(self.name)

    def destroy(self):
        RepositoryFactory.delete(self.name)

    def list_snapshot(self):
        return SnapshotFactory.list(self.name)

    def take_snapshot(self, snap_name=None):
        if not snap_name:
            snap_name = create_default_snapshot_name()
        snap = SnapshotFactory.create(self.name, snap_name)
        return snap


class SnapshotFactory:
    path = "_snapshot"

    @classmethod
    def list(cls, repo_name):
        path = cls.path + '/' + repo_name + '/' + "_all"
        lret = []
        for snap_json in Api.get(path)["snapshots"]:
            lret.append(Snapshot(snap_json))
        return lret

    @classmethod
    def destroy(cls, repo_name, snap_name):
        path = cls.path + '/' + repo_name + '/' + snap_name
        return Api.delete(path)

    @classmethod
    def create(cls, repo_name, snap_name):
        path = cls.path + '/' + repo_name + '/' + snap_name + "?wait_for_completion=true"
        return Api.put(path, data='')


class Snapshot:
    def __init__(self, json):
        self.json = json
        self.name = json["snapshot"]
        self.is_destroyed = False

    def __repr__(self):
        return "Snapshot({})".format(self.name)


def setup():
    # check that default repository exists
    if not RepositoryFactory.get(DEFAULT_REPOSITORY_NAME):
        print_color("Default Repository does not exists.", color="warning")
        print_color("Let's create it", color="okblue")
        RepositoryFactory.create(DEFAULT_REPOSITORY_NAME)
        if RepositoryFactory.get(DEFAULT_REPOSITORY_NAME):
            print_color("Default repository created!", color="okgreen")
        else:
            print_color("Unable to create the default repository", color="error")
            print_color("Exit", color="okblue")
            sys.exit(1)


def print_status():
    # status
    dhealth = get_cluster_health()
    print_color(" - status : ", color="okblue", end="")
    if "green" == dhealth["status"]:
        color = "okgreen"
    elif "yellow" == dhealth["status"]:
        color = "warning"
    else:
        color = "fail"
    print_color(dhealth["status"], color=color)
    # unassigned_shards
    if dhealth["unassigned_shards"] == 0:
        print_color(" - unassigned_shards : ", color="okblue", end="")
        print_color("0", color="okgreen")
    else:
        print_color(" - unassigned_shards : ", color="okblue", end="")
        print_color(dhealth["unassigned_shards"], color="warning")
    # routing
    routing = RoutingAllocation.get()
    print_color(" - routing : ", color="okblue", end="")
    if "all" == routing:
        color = "okgreen"
    elif "none" == routing:
        color = "warning"
    else:
        color = "fail"
    print_color(routing, color=color, end="")
    print_color(" (all: allow redistribution (*normal*), none: disallow redistribution (*during restart*))")


# DELETE OLD INDEX
def main_index_list(show_old=False):
    if show_old:
        for index in get_lindex_to_trash():
            print(index)
    else:
        for index in get_lindex():
            print(index)

def get_lindex():
    return list(requests.get(URLBASE + "/_stats/indices").json()["indices"].keys())


def delete_index(index):
    r = requests.delete(URLBASE + "/" + index)
    if r.status_code != 200:
        print("unable to delete index({})".format(index))


def get_lindex_to_trash():
    lindex_to_trash=[]
    today=date.today()
    lindex = get_lindex()
    for index in lindex:
        # is elligible
        match=RE_DELETE_OLD_INDEX.match(index)
        if not match:
            continue
        index_date=datetime.strptime(index.split("-")[-1], "%Y.%m.%d").date()
        index_prefix="-".join(index.split("-")[:-1])
        # trash it or not
        max_age=MAX_AGE_BY_PREFIX.get(index_prefix, MAX_AGE_DEFAULT)
        if index_date+timedelta(days=max_age) < today:
            lindex_to_trash.append(index)
    return lindex_to_trash


def main_delete_index_old(force=False):
    lindex = get_lindex_to_trash()
    if not force:
        if len(lindex) > DELETE_OLD_INDEX_MAX_ELEMENTS:
            print_color("Security triggered, {} element on the list when the maximun is {}".format(len(lindex)
                                                                                                   ,
                                                                                                   DELETE_OLD_INDEX_MAX_ELEMENTS)
                        , color="fail")
            print("The list is:")
            print(" - " + "\n - ".join(lindex))
            print_color("Exit !", color="okblue")
            sys.exit(1)
    for index in lindex:
        print(index)
        delete_index(index)


# BACKUP

def backup():
    setup()
    now = datetime.now()
    snap_name = now.strftime("%Y.%m.%d_%Hh%Mm%Ss")
    # first remove all the backup
    print_color("Remove all the previous snapshot.", color="okblue")
    lsnapshot = SnapshotFactory.list(DEFAULT_REPOSITORY_NAME)
    if lsnapshot:
        for snapshot in SnapshotFactory.list(DEFAULT_REPOSITORY_NAME):
            print_color(" - {}".format(snapshot.name), color="okblue")
            SnapshotFactory.destroy(DEFAULT_REPOSITORY_NAME, snapshot.name)
    else:
        print_color(" - No snapshots to remove.", color="okgreen")
    # do a full backup
    print_color("Do a full backup.", color="okblue")
    SnapshotFactory.create(DEFAULT_REPOSITORY_NAME, snap_name)
    # check if snapshot get created successfully
    snasphot = SnapshotFactory.list(DEFAULT_REPOSITORY_NAME)[0]
    if snapshot.json["state"] == "SUCESS":
        print_color(" - OK, snapshot name is {}".format(snap_name), color="okgreen")
    else:
        print_color(" - KO", color="fail")
        raise Exception("Unable to snapshot.")


def main_reallocate_shards_unassigned():
    lshard_all = ShardFactory.get()
    lshard = [shard for shard in lshard_all if shard.state == "UNASSIGNED"]
    lshard.sort(key=operator.attrgetter("index"))

    if not lshard:
        print_color("No (0/{}) shard needs to be reassigned. Exit.".format(len(lshard_all)), color="okblue")
    # group shard by index name, and then randomly reallocate it to a node

    for index, group_shard in groupby(lshard, operator.attrgetter("index")):
        random.shuffle(LNODE)
        print("----")
        print(index)
        lshard_by_index = list(group_shard)
        i = 0
        for shard in lshard_by_index:
            node = LNODE[i]
            i += 1
            shard.reallocate(node)


def xymon():
    '''show status of elastic search by ug_estool'''
    # status
    dhealth = get_cluster_health()
    xymon_color = libcbr.xymon.Color("green")
    lout = []
    if "green" == dhealth["status"]:
        color_health = libcbr.xymon.Color("green")
        lout.append(" - &green status : green")
    elif "yellow" == dhealth["status"]:
        color_health = libcbr.xymon.Color("yellow")
        lout.append(" - &yellow status: yellow")
    else:
        color_health = libcbr.xymon.Color("red")
        lout.append(" - &red status: red")
    # unassigned_shards
    if dhealth["unassigned_shards"] == 0:
        color_unassigned_shards = libcbr.xymon.Color("green")
        lout.append(" - &green unassigned_shards: 0")
    else:
        color_unassigned_shards = libcbr.xymon.Color("yellow")
        lout.append(" - &yellow unassigned_shards: {}".format(dhealth["unassigned_shards"]))
    # routing
    routing = RoutingAllocation.get()
    if "all" == routing:
        color_routing = libcbr.xymon.Color("green")
        lout.append(" - &green routing : all")
    elif "none" == routing:
        color_routing = libcbr.xymon.Color("yellow")
        lout.append(" - &yellow routing : none")
    else:
        color_routing = libcbr.xymon.Color("red")
        lout.append(" - &red routing : {}".format(routing))
    #
    xymon_color=color_health+color_unassigned_shards+color_routing
    lout.insert(0, "&{} Elastic :".format(xymon_color))
    return (xymon_color, lout)


def main():
    parser = argparse.ArgumentParser(description='tools to manage Elastic Search')
    subparsers = parser.add_subparsers(dest="sub", help="sub-command help")
    # status
    status_parser = subparsers.add_parser("status", help="show the state of Elastic Search")
    # xymon
    xymon_parser = subparsers.add_parser("xymon", help="provide data to give to xymon")
    # reroute shard
    reroute_parser = subparsers.add_parser("reallocate-unassigned-shards", help="reroute shards")
    # backup
    reroute_parser = subparsers.add_parser("backup", help="backup")

    # index
    index_parser=subparsers.add_parser("index", help="manage index")
    index_subparsers=index_parser.add_subparsers(help="index-command-help")
    #  list
    indexlist_parser=index_subparsers.add_parser(name="list", help="list indexes")
    indexlist_parser.set_defaults(name="index_list")
    indexlist_parser.add_argument("--old", help="old to be detroyed", action="store_true")
    #  delete-old
    indexdelete_parser=index_subparsers.add_parser(name="delete-old", help="delete old indexes")
    indexdelete_parser.set_defaults(name="index_delete")
    indexdelete_parser.add_argument("--force", action="store_true")

    #
    args = parser.parse_args()

    if "status" == args.sub:
        print_status()
    elif "xymon" == args.sub:
        color, lxymon_out = xymon()
        print("color: {}".format(color))
        print("lout:")
        print(os.linesep.join(lxymon_out))
    elif "reallocate-unassigned-shards" == args.sub:
        main_reallocate_shards_unassigned()
    elif "backup" == args.sub:
        backup()
    elif "index_list" == args.name:
        main_index_list(show_old=args.old)
    elif "index_delete" == args.name:
        main_delete_index_old(force=args.force)
    else:
        parser.print_usage()

if __name__ == '__main__':
    main()
