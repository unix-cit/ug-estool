from setuptools import setup

setup(
    name='ug-estool',
    version="0.0.5",
    author="BRINER Cédric",
    author_email="cedric.briner@unige.ch",
    description="tools to manage our elasticsearch",
    license = "gpl3 and above",
#    zip_safe = False,
#    py_modules= ['ug_estool'],
    packages = ["ug_estool"],
#    install_requires = ["libcbr", "ug_xymon_malert"],
    install_requires = ["libcbr"],
    entry_points={
        'console_scripts': ['ug-estool = ug_estool:main'],
        'ch.unige.central_it.xymon.malert': ['estool = ug_estool:xymon']
    }
)
